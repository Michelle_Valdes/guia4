import java.util.ArrayList;
import java.util.Random;

public class Automovil {

	private Motor motor;
	private Velocimetro velocimetro;
	private ArrayList<Rueda> rueda;
	private Estanque estanque;
	private Boolean encendido;
	private Random rd = new Random();
    private Double kilometraje;

	Automovil(){
		this.rueda = new ArrayList<Rueda>();
		this.encendido = false;
        this.kilometraje = 0.0;
	}

	public Motor getMotor() {
		return motor;
	}

	public void setMotor(Motor motor) {
		this.motor = motor;
		System.out.println("El automovil tiene un motor de " + String.valueOf(this.motor.getCilindrada()));
	}

	public Velocimetro getVelocimetro() {
		return velocimetro;
	}

	public void setVelocimetro(Velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}

	public Estanque getEstanque() {
		return estanque;
	}

	public void setEstanque(Estanque estanque) {
		this.estanque = estanque;
	}

	public Boolean getEncendido() {
		return encendido;
	}

	public void setEncendido(Boolean encendido) {
		this.encendido = encendido;
	}

	public ArrayList<Rueda> getRueda() {
		return rueda;
	}

	public void setRueda(Rueda rueda) {
		this.rueda.add(rueda);
	}

	public void enciende_automovil() {

		System.out.println("Se ha encendido el auto");
		this.encendido = true;
		this.estanque.setVolumen(0.32);
	}

    public void apagar_auto(){
        System.out.println("Se ha apagado el auto");
        this.encendido = false;
    }

	public void movimiento() {

		Double tiempo = Double.valueOf(rd.nextInt(10)+1); 
		Double velocidad = 120.0; 
		Double desplazamiento = Double.valueOf(tiempo * velocidad / 3600);
		Double consumo;

		// Consumo para cilindrada pequeña
		if (this.motor.getCilindrada() == 1.2){
			consumo = desplazamiento / 20; 
		}
		// Consumo para cilindrada grande
		else{
			consumo = desplazamiento / 14; 
		}
        System.out.println("Consumo de este movimiento: " + String.valueOf(consumo));

		this.estanque.setVolumen(consumo);

		for (Rueda r: this.rueda){
			r.setDesgaste(rd.nextInt(10)+1);
		}

		System.out.println("El auto se mueve");
        System.out.println("El movimiento ha durado " + String.valueOf(tiempo) 
                           + " segundos");

        this.kilometraje += desplazamiento;
	}

	public void chequeo_ruedas() {

		int i = 1;
        System.out.println("Integridad de Ruedas:");
		for (Rueda r : this.rueda) {
			System.out.println("Rueda " + 
                               String.valueOf(i) + 
                               ": " + 
                               String.valueOf(r.getDesgaste()));
			i++;
		}
	}
    //muestra el estado en que se encuentra el automovil
	public void reporte_automovil() {
        System.out.println("+----------------------------------------------+");
		System.out.println("Combustible Actual: " + this.estanque.getVolumen());
		System.out.println("Velocidad Actual: " + this.velocimetro.getVelocidadMaxima());
        System.out.println("Kilometráje: " +
                           String.valueOf(this.kilometraje) + " kilometros");
        chequeo_ruedas();
        System.out.println("+----------------------------------------------+");
	}

}
