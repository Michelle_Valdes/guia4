import  java.io.BufferedReader;
import  java.io.IOException;
import  java.io.InputStreamReader;
import  java.util.ArrayList;

public class Controlador {

    private ArrayList<Rueda> aux; 

	Controlador() throws IOException{

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		Automovil auto = new Automovil();
        aux = new ArrayList<Rueda>();

		
		Motor motor = new Motor();

		auto.setMotor(motor);

		Velocimetro velocimetro = new Velocimetro();
		auto.setVelocimetro(velocimetro);

		Estanque estanque = new Estanque();
		auto.setEstanque(estanque);

		
		for (int i=0; i<4;i++) {
			auto.setRueda(new Rueda());
		}

		auto.reporte_automovil();
		auto.enciende_automovil();
        
        String opcion;
        while(true){
            System.out.println("Ingrese 'm' para moverse (o 'c' para salir)");
            opcion = reader.readLine();

            if (opcion.equals("m")){
                auto.movimiento();
                auto.reporte_automovil();

                for(Rueda r: auto.getRueda()){
                    if(r.getDesgaste() < 10){
                        System.out.println("Se debe cambiar una rueda");
                        auto.apagar_auto();
                        aux.add(r);
                    }
                }

                for(Rueda r: aux){
                    auto.getRueda().remove(r);
                    auto.getRueda().add(new Rueda());
                    auto.enciende_automovil();
                }
                aux.clear();
            }

            else if (opcion.equals("c")){
                System.out.println("Adios");
                break;
            }

            if (auto.getEstanque().getVolumen() <= 0){
                System.out.println("Al auto se le ha acabado el combustible");
                break;
            }
        }

	}
}
