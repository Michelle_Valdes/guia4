import java.util.Random;

public class Motor {

	private Double cilindrada;

	Motor(Double cilindrada){

		this.cilindrada = cilindrada;
	}

	Motor(){
		//Random para determinar la cilindrada 
		Double cilindrada[] = new Double[]{1.2, 1.6};
		this.cilindrada = cilindrada[new Random().nextInt(cilindrada.length)];
	}

	public Double getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(Double cilindrada) {
		this.cilindrada = cilindrada;
	}

}
